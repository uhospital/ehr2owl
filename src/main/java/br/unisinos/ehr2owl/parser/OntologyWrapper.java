package br.unisinos.ehr2owl.parser;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.openehr.rm.support.basic.Interval;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeRestriction;
import org.semanticweb.owlapi.model.OWLFacetRestriction;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.util.AutoIRIMapper;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.vocab.OWLFacet;

public class OntologyWrapper {

	OWLOntologyManager manager;
	OWLDataFactory factory;
	OWLOntology ontology;
	PrefixManager prefixManager;
	
	public OntologyWrapper(String ontologyName) throws OWLOntologyCreationException {
		manager = OWLManager.createOWLOntologyManager();
		factory = manager.getOWLDataFactory();
		
		IRI ontologyIRI = IRI.create("http://uhospital.unisinos.br/openehr/", ontologyName);
		ontology = manager.createOntology(ontologyIRI);

		AutoIRIMapper repoMapper = new AutoIRIMapper(new File("openehr-rm"), false);
		manager.getIRIMappers().add(repoMapper);
		
		OWLImportsDeclaration importDeclaration = factory.getOWLImportsDeclaration(IRI.create("http://uhospital.unisinos.br/openehr/ehr"));
		manager.applyChange(new AddImport(ontology, importDeclaration));
		manager.loadOntology(IRI.create("http://uhospital.unisinos.br/openehr/ehr"));
		
		prefixManager = new DefaultPrefixManager();
		prefixManager.setDefaultPrefix("http://uhospital.unisinos.br/openehr/blood-pressure.owl#");
		prefixManager.setPrefix("rm", "http://uhospital.unisinos.br/openehr/ehr#");
		prefixManager.setPrefix("dsrm", "http://uhospital.unisinos.br/openehr/data_structures#");
		prefixManager.setPrefix("dtrm", "http://uhospital.unisinos.br/openehr/data_types#");
		prefixManager.setPrefix("srm", "http://uhospital.unisinos.br/openehr/support#");
	}
	
	public void save() throws OWLOntologyStorageException {
		manager.saveOntology(ontology, System.out);
	}
	
	public OWLClass getClass(String className) {
		return factory.getOWLClass(className, prefixManager);
	}
	
	public void setSubClassOf(OWLClass childClass, OWLClassExpression classExp) {
		OWLSubClassOfAxiom ax = factory.getOWLSubClassOfAxiom(childClass, classExp);
		manager.addAxiom(ontology, ax);
	}
	
	public OWLClass specializeClass(String childName, String parentName) {
		OWLClass parentClass = getClass(parentName);
		OWLClass childClass = getClass(childName);
		setSubClassOf(childClass, parentClass);
		return childClass;
	}
	
	public void setNodeId(String value, OWLClass cls) {
		OWLAnnotationProperty property = factory.getOWLAnnotationProperty("NodeID", prefixManager);
		OWLAnnotation annotation = factory.getOWLAnnotation(property, factory.getOWLLiteral(value));
		OWLAnnotationAssertionAxiom ax = factory.getOWLAnnotationAssertionAxiom(cls.getIRI(), annotation);
		manager.addAxiom(ontology, ax);
	}

	public void setAllValuesRestriction(OWLClass restrictedClass, String propertyName, OWLClassExpression classExp) {
		OWLObjectProperty property = factory.getOWLObjectProperty(propertyName, prefixManager);
		OWLObjectAllValuesFrom restriction = factory.getOWLObjectAllValuesFrom(property, classExp);
		setSubClassOf(restrictedClass, restriction);
	}
	
	public OWLObjectUnionOf createOWLUnionOf(Set<? extends OWLClassExpression> operands) {
		return factory.getOWLObjectUnionOf(operands);
	}
	
	public OWLObjectIntersectionOf createOWLIntersectionOf(Set<OWLClassExpression> operands) {
		return factory.getOWLObjectIntersectionOf(operands);
	}
	
	public OWLDataHasValue createOWLHasValue(String propertyName, String value) {
		OWLDataProperty property = factory.getOWLDataProperty(propertyName, prefixManager);
		return factory.getOWLDataHasValue(property, factory.getOWLLiteral(value));
	}
	
	public OWLDatatype getDoubleDatatype() {
		return factory.getDoubleOWLDatatype();
	}
	
	public OWLDatatype getIntegerDatatype() {
		return factory.getIntegerOWLDatatype();
	}
		
	@SuppressWarnings("rawtypes")
	public OWLDataAllValuesFrom specializeDatatype(String propertyName, Interval interval, OWLDatatype datatype) {
		OWLDataProperty property = factory.getOWLDataProperty(propertyName, prefixManager);
		Object lower = interval.getLower();
		Object upper = interval.getUpper();
		Set<OWLFacetRestriction> facets = new HashSet<OWLFacetRestriction>();
		if (lower != null) {
			if (interval.isLowerIncluded())
				facets.add(factory.getOWLFacetRestriction(OWLFacet.MIN_INCLUSIVE, factory.getOWLLiteral(lower.toString(), datatype)));
			else
				facets.add(factory.getOWLFacetRestriction(OWLFacet.MIN_EXCLUSIVE, factory.getOWLLiteral(lower.toString(), datatype)));
		}
		if (upper != null) {
			if (interval.isUpperIncluded())
				facets.add(factory.getOWLFacetRestriction(OWLFacet.MAX_INCLUSIVE, factory.getOWLLiteral(upper.toString(), datatype)));
			else
				facets.add(factory.getOWLFacetRestriction(OWLFacet.MAX_EXCLUSIVE, factory.getOWLLiteral(upper.toString(), datatype)));
		}
		OWLDatatypeRestriction dataRange = factory.getOWLDatatypeRestriction(datatype, facets);
		return factory.getOWLDataAllValuesFrom(property, dataRange);
	}

	public void setComment(OWLClass cls, String comment) {
		OWLAnnotationProperty property = factory.getRDFSComment();
		OWLAnnotation annotation = factory.getOWLAnnotation(property, factory.getOWLLiteral(comment));
		OWLAnnotationAssertionAxiom ax = factory.getOWLAnnotationAssertionAxiom(cls.getIRI(), annotation);
		manager.addAxiom(ontology, ax);
	}

	public OWLClass createOWLList(String parentName, Set<OWLClass> operands) {
		String childName = "List";
		for (Iterator<OWLClass> it = operands.iterator(); it.hasNext();) {
			OWLClass cls = it.next();
			childName += "_" + cls.getIRI().getShortForm();
		}
		OWLClass childList = specializeClass(childName, parentName);
		setAllValuesRestriction(childList, "srm:hasContent", createOWLUnionOf(operands));
		setAllValuesRestriction(childList, "srm:hasNext", childList);
		return childList;
	}
}
