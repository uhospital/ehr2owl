package br.unisinos.ehr2owl.parser;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.ontology.ArchetypeOntology;
import org.openehr.am.archetype.ontology.ArchetypeTerm;
import org.openehr.am.archetype.ontology.OntologyDefinitions;

public class ArchetypeUtils {

	public static String getTermDefinitionFor(String language, String code, Archetype archetype) {
		ArchetypeOntology ontology = archetype.getOntology();
		List<OntologyDefinitions> ontologyDefs = ontology.getTermDefinitionsList();
		Iterator<OntologyDefinitions> it1 = ontologyDefs.iterator();
		while (it1.hasNext()) {
			OntologyDefinitions defs = it1.next();
			if (defs.getLanguage().equals(language)) {
				List<ArchetypeTerm> terms = defs.getDefinitions();
				Iterator<ArchetypeTerm> it2 = terms.iterator();
				while (it2.hasNext()) {
					ArchetypeTerm term = it2.next();
					if (term.getCode().equals(code)) {
						return term.getText();
					}
				}
			}
		}
		return null;
	}
}
