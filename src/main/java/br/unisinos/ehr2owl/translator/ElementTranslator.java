package br.unisinos.ehr2owl.translator;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.openehr.am.openehrprofile.datatypes.quantity.CDvQuantity;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class ElementTranslator extends Translator {

	public ElementTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dsrm:ELEMENT");
		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("value")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = (CObject) it2.next();
					if (obj instanceof CComplexObject) {
						if (obj.getRmTypeName().equals("DV_TEXT")) {
							TextTranslator translator = new TextTranslator (archetype, ontology, obj, this);
							translator.translate();
							ontology.setAllValuesRestriction(owlCls, "dsrm:value", translator.getResult());
						} else if (obj.getRmTypeName().equals("DV_CODED_TEXT")) {
							CodedTextTranslator translator = new CodedTextTranslator (archetype, ontology, obj, this);
							translator.translate();
							ontology.setAllValuesRestriction(owlCls, "dsrm:value", translator.getResult());
						}
					} else if (obj instanceof CDvQuantity) {
						QuantityTranslator translator = new QuantityTranslator(archetype, ontology, obj, this);
						translator.translate();
						ontology.setAllValuesRestriction(owlCls, "dsrm:value", translator.getResult());
					}
					
				}
			}
		}
	}

}
