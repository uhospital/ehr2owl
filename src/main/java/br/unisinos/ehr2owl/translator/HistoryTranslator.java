package br.unisinos.ehr2owl.translator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CMultipleAttribute;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.semanticweb.owlapi.model.OWLClassExpression;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class HistoryTranslator extends Translator {

	public HistoryTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("events")) {
				List<CObject> children = attribute.getChildren();
				if (attribute instanceof CMultipleAttribute && !((CMultipleAttribute)attribute).getCardinality().isUnique()) {
					owlCls = specializeClass("dsrm:HISTORY");
					Set<OWLClassExpression> operands = new HashSet<OWLClassExpression>();
					for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
						CObject obj = it2.next();
						Translator translator = null;
						if (obj.getRmTypeName().equals("EVENT")) {
							translator = new EventTranslator(archetype, ontology, obj, this);
						} else if (obj.getRmTypeName().equals("INTERVAL_EVENT")) {
							translator = new IntervalEventTranslator(archetype, ontology, obj, this);
						}
						if (translator != null) {
							translator.translate();
							operands.add(translator.getResult());
						}
					}
					ontology.setAllValuesRestriction(owlCls, "dsrm:events", ontology.createOWLUnionOf(operands));
				}
			}
		}
	}

}
