package br.unisinos.ehr2owl.translator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.semanticweb.owlapi.model.OWLClass;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class ItemTreeTranslator extends Translator {

	public ItemTreeTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dsrm:ITEM_TREE");
		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("items")) {
				List<CObject> children = attribute.getChildren();
				Set<OWLClass> operands = new HashSet<OWLClass>();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					if (obj.getRmTypeName().equals("ELEMENT")) {
						ElementTranslator translator = new ElementTranslator(archetype, ontology, obj, this);
						translator.translate();
						operands.add(translator.getResult());
					}
				}
				OWLClass owlList = ontology.createOWLList("dsrm:List_ITEM", operands);
				ontology.setAllValuesRestriction(owlCls, "dsrm:items", owlList);
			}
		}
	}

}
