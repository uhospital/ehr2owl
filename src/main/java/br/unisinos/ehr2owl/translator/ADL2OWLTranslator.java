package br.unisinos.ehr2owl.translator;

import org.openehr.am.archetype.Archetype;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class ADL2OWLTranslator extends Translator {

	public ADL2OWLTranslator(Archetype archetype, OntologyWrapper ontology) {
		super(archetype, ontology, archetype.getDefinition(), null);
	}

	@Override
	public void translate() {
		if (object.getRmTypeName().equals("OBSERVATION")) {
			ObservationTranslator translator = new ObservationTranslator(archetype, ontology, object, null);
			translator.translate();
		}
	}

}
