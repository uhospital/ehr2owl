package br.unisinos.ehr2owl.translator;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class CodedTextTranslator extends Translator {

	public CodedTextTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dtrm:DV_CODED_TEXT");
		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("defining_code")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					CodePharseTranslator translator = new CodePharseTranslator(archetype, ontology, obj, this);
					translator.translate();
					ontology.setAllValuesRestriction(owlCls, "dtrm:defining_code", translator.getResult());
				}
			}
		}
	}

}
