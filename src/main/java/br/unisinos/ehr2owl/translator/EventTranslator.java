package br.unisinos.ehr2owl.translator;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class EventTranslator extends Translator {

	public EventTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {
		owlCls = specializeClass("dsrm:EVENT");
		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("data")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					if (obj.getRmTypeName().equals("ITEM_TREE")) {
						ItemTreeTranslator translator = new ItemTreeTranslator(archetype, ontology, obj, this);
						translator.translate();
						ontology.setAllValuesRestriction(owlCls, "dsrm:data", translator.getResult());
					}
				}
			} else if (attribute.getRmAttributeName().equals("state")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					if (obj.getRmTypeName().equals("ITEM_TREE")) {
						ItemTreeTranslator translator = new ItemTreeTranslator(archetype, ontology, obj, this);
						translator.translate();
						ontology.setAllValuesRestriction(owlCls, "dsrm:state", translator.getResult());
					}
				}
			}
		}
	}

}
