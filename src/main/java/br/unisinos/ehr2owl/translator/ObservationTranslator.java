package br.unisinos.ehr2owl.translator;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class ObservationTranslator extends Translator {

	public ObservationTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {
		owlCls = specializeClass("rm:OBSERVATION");
		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("data") || attribute.getRmAttributeName().equals("protocol")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					Translator translator = null;
					if (obj.getRmTypeName().equals("HISTORY")) {
						translator = new HistoryTranslator(archetype, ontology, obj, this);
					} else if (obj.getRmTypeName().equals("ITEM_TREE")) {
						translator = new ItemTreeTranslator(archetype, ontology, obj, this);
					}
					if (translator != null) {
						translator.translate();
						if (attribute.getRmAttributeName().equals("data")) {
							ontology.setAllValuesRestriction(owlCls, "rm:data", translator.getResult());
						} else if (attribute.getRmAttributeName().equals("protocol")) {
							ontology.setAllValuesRestriction(owlCls, "rm:protocol", translator.getResult());
						}
					}
				}
			}
		}
	}

}
