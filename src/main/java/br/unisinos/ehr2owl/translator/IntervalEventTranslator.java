package br.unisinos.ehr2owl.translator;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class IntervalEventTranslator extends Translator {

	public IntervalEventTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {
		owlCls = specializeClass("dsrm:INTERVAL_EVENT");
		
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("data") ||
				attribute.getRmAttributeName().equals("state") ||
				attribute.getRmAttributeName().equals("math_function") ||
				attribute.getRmAttributeName().equals("width")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					if (obj.getRmTypeName().equals("DV_CODED_TEXT")) {
						CodedTextTranslator translator = new CodedTextTranslator(archetype, ontology, obj, this);
						translator.translate();
						ontology.setAllValuesRestriction(owlCls, "dsrm:math_function", translator.getResult());
					} else if (obj.getRmTypeName().equals("DV_DURATION")) {
						DurationTranslator translator = new DurationTranslator(archetype, ontology, obj, this);
						translator.translate();
						ontology.setAllValuesRestriction(owlCls, "dsrm:width", translator.getResult());
					}
				}
			}
		}
	}

}
