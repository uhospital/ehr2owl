package br.unisinos.ehr2owl.translator;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.semanticweb.owlapi.model.OWLClass;

import br.unisinos.ehr2owl.parser.ArchetypeUtils;
import br.unisinos.ehr2owl.parser.OntologyWrapper;

public abstract class Translator {

	Archetype archetype;
	OntologyWrapper ontology;
	CObject object;
	Translator parent;
	OWLClass owlCls;
	
	public Translator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		this.archetype = archetype;
		this.ontology = ontology;
		this.object = object;
		this.parent = parent;
	}

	public abstract void translate();
	
	public OWLClass getResult() {
		return owlCls;
	}
	
	public OWLClass specializeClass(String parentName) {
		OWLClass cls;
		String childName = null;
		String nodeId = object.getNodeId();
		if (nodeId != null) {
			childName = ArchetypeUtils.getTermDefinitionFor("en", nodeId, archetype);
			childName = childName.replace(" ", "_");
			cls = ontology.specializeClass(childName, parentName);
			ontology.setNodeId(nodeId, cls);
		} else {
			childName = parent.getResult().getIRI().getShortForm();
			childName += "_" + ontology.getClass(parentName).getIRI().getShortForm();
			cls = ontology.specializeClass(childName, parentName);
		}
		return cls;
	}

}
