package br.unisinos.ehr2owl.translator;

import java.util.Iterator;
import java.util.List;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.openehr.am.archetype.constraintmodel.CPrimitiveObject;
import org.openehr.am.archetype.constraintmodel.primitive.CDuration;
import org.openehr.rm.datatypes.quantity.datetime.DvDuration;
import org.openehr.rm.support.basic.Interval;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class DurationTranslator extends Translator {

	public DurationTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dtrm:DV_DURATION");
				
		List<CAttribute> attributes = ((CComplexObject)object).getAttributes();
		for (Iterator<CAttribute> it1 = attributes.iterator(); it1.hasNext();) {
			CAttribute attribute = it1.next();
			if (attribute.getRmAttributeName().equals("value")) {
				List<CObject> children = attribute.getChildren();
				for (Iterator<CObject> it2 = children.iterator(); it2.hasNext();) {
					CObject obj = it2.next();
					if (obj instanceof CPrimitiveObject) {
						CDuration duration = (CDuration) ((CPrimitiveObject)obj).getItem();
						Interval<DvDuration> interval = duration.getInterval();
						ontology.setSubClassOf(owlCls, ontology.createOWLHasValue("dtrm:value", interval.getLower().toString()));
					}
				}
			}
		}
	}

}
