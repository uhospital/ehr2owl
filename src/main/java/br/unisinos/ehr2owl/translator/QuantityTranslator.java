package br.unisinos.ehr2owl.translator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.openehr.am.openehrprofile.datatypes.quantity.CDvQuantity;
import org.openehr.am.openehrprofile.datatypes.quantity.CDvQuantityItem;
import org.openehr.rm.support.basic.Interval;
import org.semanticweb.owlapi.model.OWLClassExpression;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class QuantityTranslator extends Translator {

	public QuantityTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dtrm:DV_QUANTITY");
		
		Set<OWLClassExpression> unionClass = new HashSet<OWLClassExpression>();
		CDvQuantity quantity = (CDvQuantity) object;
		for (Iterator<CDvQuantityItem> iterator = quantity.getList().iterator(); iterator.hasNext();) {
			CDvQuantityItem quantityItem = iterator.next();
			String units = quantityItem.getUnits();
			Interval<Double> magnitude = quantityItem.getMagnitude();
			Interval<Integer> precision = quantityItem.getPrecision();
			Set<OWLClassExpression> intersectionClass = new HashSet<OWLClassExpression>();
			
			if (units != null) {
				intersectionClass.add(ontology.createOWLHasValue("dtrm:units", units));
			}
			
			if (magnitude != null) {
				intersectionClass.add(ontology.specializeDatatype("dtrm:magnitude", magnitude, ontology.getDoubleDatatype()));
			}
			
			if (precision != null) {
				intersectionClass.add(ontology.specializeDatatype("dtrm:precision", precision, ontology.getIntegerDatatype()));
			}
			
			unionClass.add(ontology.createOWLIntersectionOf(intersectionClass));
		}
		
		ontology.setSubClassOf(owlCls, ontology.createOWLUnionOf(unionClass));
	}

}
