package br.unisinos.ehr2owl.translator;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CObject;

import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class TextTranslator extends Translator {

	public TextTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dtrm:DV_TEXT");
	}

}
