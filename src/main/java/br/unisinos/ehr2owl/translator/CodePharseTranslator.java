package br.unisinos.ehr2owl.translator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CObject;
import org.openehr.am.openehrprofile.datatypes.text.CCodePhrase;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;

import br.unisinos.ehr2owl.parser.ArchetypeUtils;
import br.unisinos.ehr2owl.parser.OntologyWrapper;

public class CodePharseTranslator extends Translator {

	public CodePharseTranslator(Archetype archetype, OntologyWrapper ontology, CObject object, Translator parent) {
		super(archetype, ontology, object, parent);
	}

	@Override
	public void translate() {		
		owlCls = specializeClass("dtrm:CODE_PHRASE");
		
		List<String> codes = ((CCodePhrase)object).getCodeList();
		Set<OWLClassExpression> operands = new HashSet<OWLClassExpression>();
		String comment = "";
		for (Iterator<String> iterator = codes.iterator(); iterator.hasNext();) {
			String code = iterator.next();
			OWLDataHasValue hasValue = ontology.createOWLHasValue("dtrm:code_string", code);
			operands.add(hasValue);
			
			String definition = ArchetypeUtils.getTermDefinitionFor("en", code, archetype);
			comment += code + " -> " + definition + ", ";
		}
		OWLObjectUnionOf unionOf = ontology.createOWLUnionOf(operands);
		ontology.setSubClassOf(owlCls, unionOf);
		ontology.setComment(owlCls, comment.substring(0, comment.length() - 3));
	}

}
