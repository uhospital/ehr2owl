package br.unisinos.ehr2owl;

import java.io.File;

import org.openehr.am.archetype.Archetype;

import br.unisinos.ehr2owl.parser.ArchetypeUtils;
import br.unisinos.ehr2owl.parser.OntologyWrapper;
import br.unisinos.ehr2owl.translator.ADL2OWLTranslator;
import se.acode.openehr.parser.ADLParser;
import se.acode.openehr.parser.ParseException;

public class App {

	public static void main(String[] args) throws ParseException, Exception {
		String fileName = "archetype.adl";
		File file = new File(fileName);
		ADLParser parser = new ADLParser(file);
		Archetype archetype = parser.parse();
		
		String ontologyName = ArchetypeUtils.getTermDefinitionFor("en", archetype.getConcept(), archetype);
		ontologyName = ontologyName.replace(" ", "_");
		ontologyName = ontologyName.toLowerCase();
		OntologyWrapper ontology = new OntologyWrapper(ontologyName);
		
		ADL2OWLTranslator translator = new ADL2OWLTranslator(archetype, ontology);
		translator.translate();
		
		ontology.save();
	}

}
